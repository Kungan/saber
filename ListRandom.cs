﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Saber
{
	public class ListRandom : IEnumerable<ListNode>
	{
		public ListNode Head;
		public ListNode Tail;
		public int Count;

		private readonly Random random = new();

		/// <summary>
		/// Добавляет новый узел и назначает ему соответствующий случайный узел
		/// </summary>
		/// <param name="data">Данные, сохраняемые в узле.</param>
		public void Add(string data)
		{
			ListNode node = BaseAdd(data);
			node.Random = GetRandomNode();
		}

		/// <summary>
		/// Добавляет новый узел, но не назначает ему соответствующий случайный узел.
		/// </summary>
		/// <param name="data">Данные, сохраняемые в узле.</param>
		/// <returns></returns>
		private ListNode BaseAdd(string data)
		{
			ListNode node = new() {
				Previous = Tail,
				Data = data
			};

			Head ??= node;
			if (Tail != null)
			{
				Tail.Next = node;
			}

			Tail = node;
			Count++;

			return node;
		}

		private ListNode GetRandomNode()
		{
			int nodeIndex = random.Next(Count);
			ListNode currentNode = Head;
			for (int i = 1; i < nodeIndex; i++)
			{
				currentNode = currentNode.Next;
			}

			return currentNode;
		}

		public void Serialize(Stream s)
		{
			if (!s.CanWrite)
			{
				throw new SerializationException("Переданный поток не поддерживает запись.");
			}

			ThrowIfNotСonsistent();

			List<ListNode> nodes = this.ToList();
			IEnumerable<SerializableNode> serializableNodes = nodes.Select(node => new SerializableNode
			{
				Previous = nodes.IndexOf(node.Previous),
				Next = nodes.IndexOf(node.Next),
				Random = nodes.IndexOf(node.Random),
				Data = node.Data
			});

			s.Write(BitConverter.GetBytes(Count));
			foreach (SerializableNode serializableNode in serializableNodes)
			{
				serializableNode.Serialize(s);
			}
		}

		public static ListRandom Deserialize(Stream s)
		{
			if (!s.CanRead)
			{
				throw new SerializationException("Переданный поток не поддерживает чтение.");
			}

			ListRandom nodes = new();
			int count = new Serializer(s).GetInt();

			Dictionary<ListNode, int> nodeToRandomNodeIndex = new();
			for (int i = 0; i < count; i++)
			{
				SerializableNode serializableNode = SerializableNode.Deserialize(s);
				ListNode node = nodes.BaseAdd(serializableNode.Data);
				nodeToRandomNodeIndex.Add(node, serializableNode.Random);
			}

			List<ListNode> nodesList = nodes.ToList();
			foreach ((ListNode node, int randomNodeIndex) in nodeToRandomNodeIndex)
			{
				node.Random = randomNodeIndex == -1 ? null : nodesList[randomNodeIndex];
			}

			nodes.ThrowIfNotСonsistent();

			return nodes;
		}

		/// <summary>
		/// Проверяет на список на консистентность. При желании можно добавить дополнительные проверки.
		/// </summary>
		private void ThrowIfNotСonsistent()
		{
			List<ListNode> nodes = this.ToList();
			if (Count != nodes.Count)
			{
				throw new InconsistentStateException(
					$"Значение свойства {nameof(Count)} не совпадает с количеством элементов в списке"
				);
			}
		}

		public IEnumerator<ListNode> GetEnumerator()
		{
			ListNode current = Head;
			while (current != null)
			{
				yield return current;
				current = current.Next;
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
