﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Saber
{
	/// <summary>
	/// Сериализуемое представление узла. Ссылки на другие узлы заменены их индексами.
	/// </summary>
	class SerializableNode
	{
		public SerializableNode()
		{
			List<ValidationResult> results = new();
			ValidationContext context = new(this);
			if (!Validator.TryValidateObject(this, context, results, true))
			{
				throw new SerializationException(string.Join("\r\n", results.Select(result => result.ErrorMessage)));
			}
		}

		[Range(-1, int.MaxValue)]
		public int Previous { get; init; }

		[Range(-1, int.MaxValue)]
		public int Next { get; init; }

		[Range(0, int.MaxValue)]
		public int Random { get; init; }

		public string Data { get; init; }

		public void Serialize(Stream stream)
		{
			new Serializer(stream)
				.WriteInt(Previous)
				.WriteInt(Next)
				.WriteInt(Random)
				.WriteString(Data);
		}

		public static SerializableNode Deserialize(Stream stream)
		{
			Serializer serializer = new(stream);
			int previous = serializer.GetInt();
			int next = serializer.GetInt();
			int random = serializer.GetInt();
			string data = serializer.GetString();

			return new SerializableNode
			{
				Previous = previous,
				Next = next,
				Random = random,
				Data = data
			};
		}
	}
}
