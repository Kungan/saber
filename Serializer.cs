﻿using System;
using System.IO;
using System.Text;

namespace Saber
{
	internal class Serializer
	{
		private Stream stream;

		public Serializer(Stream stream)
		{
			this.stream = stream;
		}

		public Serializer WriteInt(int data)
		{
			stream.Write(BitConverter.GetBytes(data));
			return this;
		}

		public int GetInt()
		{
			byte[] buffer = new byte[sizeof(int)];
			stream.Read(buffer, 0, buffer.Length);
			return BitConverter.ToInt32(buffer);
		}

		public Serializer WriteString(string data)
		{
			byte[] buffer = Encoding.Unicode.GetBytes(data);
			stream.Write(BitConverter.GetBytes(buffer.Length));
			stream.Write(buffer);
			return this;
		}

		public string GetString()
		{
			int dataLength = GetInt();
			byte[] buffer = new byte[dataLength];
			stream.Read(buffer, 0, buffer.Length);
			return Encoding.Unicode.GetString(buffer);
		}
	}
}