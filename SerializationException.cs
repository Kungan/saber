﻿using System;

namespace Saber
{
	internal class SerializationException : Exception
	{
		public SerializationException(string message) : base(message)
		{
		}
	}
}