﻿using Microsoft.Win32;
using System.Collections.Generic;
using System.Linq;

namespace Saber.Services
{
	/// <summary>
	/// Сервис работы с файлами.
	/// </summary>
	public class FileService
	{
		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="extensions"> /// Расширения файлов. Ключ - отображаемое название фильтра.
		/// Значение - массив соответствующих расширений (без звездочки).</param>
		/// <param name="doAddAllFilesFilter">Добавлять ли возможность отображения всех файлов.</param>
		public FileService(Dictionary<string, string[]> extensions, bool doAddAllFilesFilter = true)
		{
			string filter = CreateFilterString(extensions, doAddAllFilesFilter);

			SaveFileDialog = new SaveFileDialog
			{
				Filter = filter
			};

			OpenFileDialog = new OpenFileDialog
			{
				Filter = filter
			};

		}

		protected OpenFileDialog OpenFileDialog { get; set; }

		protected SaveFileDialog SaveFileDialog { get; set; }

		/// <summary>
		/// Открывает диалог сохранения файла.
		/// </summary>
		/// <param name="defaultFileName">Название файла по умолчанию.</param>
		/// <returns>Выбранное название. Либо <see langword="null"/>, если выбор был отменен.</returns>
		public string GetFileNameToSave(string defaultFileName = "")
		{
			return GetFileName(SaveFileDialog, defaultFileName);
		}

		/// <summary>
		/// Открывает диалог открытия файла.
		/// </summary>
		/// <param name="defaultFileName">Название файла по умолчанию.</param>
		/// <returns>Выбранное название. Либо <see langword="null"/>, если выбор был отменен.</returns>
		public string GetFileNameToOpen(string defaultFileName = "")
		{
			return GetFileName(OpenFileDialog, defaultFileName);
		}

		/// <summary>
		/// Открывает файловый диалог.
		/// </summary>
		/// <param name="dialog">Объект диалога.</param>
		/// <param name="defaultFileName">Выбранное название или <see langword="null"/>, если выбор был отменен. </param>
		/// <returns></returns>
		public static string GetFileName(FileDialog dialog, string defaultFileName)
		{
			dialog.FileName = defaultFileName;
			if (dialog.ShowDialog() ?? false) {
				return dialog.FileName;
			}
			return null;
		}

		/// <summary>
		/// Создает строку фильтра для открытия файлов.
		/// </summary>
		/// <param name="extensions">Словарь фильтров. Ключ - отображаемое название фильтра. Значение - массив соответствующих расширений.</param>
		/// <param name="doAddAllFilesFilter">Добавлять ли возможность отображения всех файлов.</param>
		/// <returns>
		/// Строка вида 
		/// "Изображения (*.bmp;*.gif;*.jpg;*.jpeg;*.png;*.tiff)|*.bmp;*.gif;*.jpg;*.jpeg;*.png;*.tiff|Все файлы (*.*)|*.*"
		/// </returns>
		private static string CreateFilterString(Dictionary<string, string[]> extensions, bool doAddAllFilesFilter)
		{
			Dictionary<string, string[]> modifiedExtentions = new(extensions);
			if (doAddAllFilesFilter) {
				// Добавляем возможность фильтрации по всем файлам
				modifiedExtentions.Add("Все файлы", new[] { "*" });
			}

			return string.Join( "|",
				modifiedExtentions.Select(pair =>
				{
					(string description, string[] extensionsList) = pair;
					string extensionGroup = string.Join( ";",
						extensionsList.Select(extension => $"*.{extension}")
					);
					return $"{description}  | {extensionGroup}";
				}).ToArray()
			);
		}
	}
}
