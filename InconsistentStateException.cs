﻿using System;

namespace Saber
{
	internal class InconsistentStateException : Exception
	{
		public InconsistentStateException(string message): base(message)
		{
		}
	}
}