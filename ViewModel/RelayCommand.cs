﻿using System;
using System.Windows.Input;

namespace Saber.ViewModel
{
	/// <summary>
	/// Класс команды.
	/// </summary>
	public class RelayCommand : ICommand
	{
		private readonly Action<object> executeAction;
		private readonly Predicate<object> canExecuteAction;

		public RelayCommand(Action<object> execute)
			: this(execute, _ => true)
		{
		}

		public RelayCommand(Action<object> action, Predicate<object> canExecute)
		{
			executeAction = action;
			canExecuteAction = canExecute;
		}

		public bool CanExecute(object parameter)
		{
			return canExecuteAction(parameter);
		}

		public event EventHandler CanExecuteChanged
		{
			add => CommandManager.RequerySuggested += value;
			remove => CommandManager.RequerySuggested -= value;
		}

		public void Execute(object parameter)
		{
			executeAction(parameter);
		}
	}
}
