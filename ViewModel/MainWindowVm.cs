﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using QuickGraph;
using Saber.Services;

namespace Saber.ViewModel
{
	/// <summary>
	/// Модель представления главного окна приложения.
	/// </summary>
	internal class MainWindowVm : BaseViewModel
	{
		private readonly FileService fileService = new(
			new Dictionary<string, string[]> {
			{ "Двоичные данные", new[]{"data"} }
		});

		private readonly Dictionary<ListNode, string> labels = new();
		private int currentNodeIndex = 0;
		private ListRandom randomList = new();
		private string contentToAdd = "";
		private BidirectionalGraph<object, IEdge<object>> graph = new();

		/// <summary>
		///	Команда выхода из приложения
		/// </summary>
		public RelayCommand QuitCommand { get; }

		/// <summary>
		///	Команда сохранения.
		/// </summary>
		public RelayCommand SaveCommand { get; }

		/// <summary>
		/// Команда открытия файла.
		/// </summary>
		public RelayCommand LoadCommand { get; }

		/// <summary>
		///	Команда добавления нового элемента.
		/// </summary>
		public RelayCommand AddCommand { get; }

		/// <summary>
		/// Модель представления графа для визуализации двусвязного списка.
		/// </summary>
		public BidirectionalGraph<object, IEdge<object>> Graph
		{
			get => graph;
			private set
			{
				graph = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// Данные для нового узла
		/// </summary>
		public string ContentToAdd
		{
			get => contentToAdd;
			set
			{
				contentToAdd = value;
				OnPropertyChanged();
			}
		}

		public MainWindowVm()
		{
			SaveCommand = new RelayCommand(_ => SaveModelCommand_Executed());
			QuitCommand = new RelayCommand(_ => QuitCommand_Executed());
			AddCommand = new RelayCommand(_ => AddCommand_Executed());
			LoadCommand = new RelayCommand(_ => LoadModelCommand_Executed());
		}

		private void AddCommand_Executed()
		{
			randomList.Add(ContentToAdd);
			ListNode tail = randomList.Tail;
			VisualizeNode(tail);
		}

		private void VisualizeNode(ListNode node)
		{
			AddVertex(node);

			ListNode previous = node.Previous;
			if (previous != null)
			{
				AddEdge(node, previous);
				AddEdge(previous, node);
			}

			ListNode random = node.Random;
			if (random != null)
			{
				AddEdge(node, random);
			}
		}

		/// <summary>
		/// Добавляет ребро в визуализацию графа. Если вершин в графе нет, они будут добавлены автоматически.
		/// </summary>
		/// <param name="start">Начальный узел.</param>
		/// <param name="end">Конечный узел</param>
		private void AddEdge(ListNode start, ListNode end)
		{
			Graph.AddEdge(new Edge<object>(GetLabel(start), GetLabel(end)));
		}

		/// <summary>
		/// Добавляет вершину в визуализацию графа. 
		/// </summary>
		/// <param name="node"></param>
		private void AddVertex(ListNode node)
		{
			Graph.AddVertex(GetLabel(node));
		}

		/// <summary>
		/// Возавращает название узла. Оно должно быть уникальным, иначе визуальный компонент не сможет их отличить.
		/// </summary>
		/// <param name="node">Узел</param>
		/// <returns>Соответствуещее ему название</returns>
		private string GetLabel(ListNode node)
		{
			if (labels.TryGetValue(node, out string label))
			{
				return label;
			}

			label = $"[{currentNodeIndex}] {node.Data}";
			labels.Add(node, label);
			currentNodeIndex++;
			return label;
		}

		/// <summary>
		/// Команда выхода из приложения.
		/// </summary>
		private static void QuitCommand_Executed()
		{
			Application.Current.Shutdown();
		}

		/// <summary>
		/// Сохранение списка в файл.
		/// </summary>
		private void SaveModelCommand_Executed()
		{
			string filename = fileService.GetFileNameToSave();
			if (filename == null)
			{
				return;
			}

			try
			{
				using FileStream stream = File.Create(filename);
				randomList.Serialize(stream);
			}
			catch (SerializationException e) {
				MessageBox.Show($"Неверный формат данных: {e.Message}");
			}
			catch (Exception e) {
				MessageBox.Show($"Ошибка при сохранении файла: {e.Message}");
			}
		}

		/// <summary>
		/// Открытие файла с сериализованным списком.
		/// </summary>
		private void LoadModelCommand_Executed()
		{
			try
			{
				string filename = fileService.GetFileNameToOpen();
				if (filename == null)
				{
					return;
				}

				using FileStream stream = File.OpenRead(filename);
				randomList = ListRandom.Deserialize(stream);
			}
			catch (SerializationException e)
			{
				MessageBox.Show($"Неверный формат данных: {e.Message}");
			}
			catch (InconsistentStateException e)
			{
				MessageBox.Show($"Сохраненный список не консистентен: {e.Message}");
			}
			catch (Exception e)
			{
				MessageBox.Show($"Ошибка при открытии файла: {e.Message}");
			}
			ClearVisualization();
			foreach (ListNode node in randomList)
			{
				VisualizeNode(node);
			}
		}

		/// <summary>
		/// Очищает визуалищзацию графа.
		/// </summary>
		private void ClearVisualization()
		{
			currentNodeIndex = 0;

			// Почему-то Graph.Clear() не работает так, как нужно.
			// Старые узлы и ребра не пропадают, только перестают выделяться.
			Graph = new();
		}
	}
}